﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuSelection : MonoBehaviour {

	bool startSelected;

	void Awake()
	{
		startSelected = true;
	}

	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyUp(KeyCode.DownArrow)){
			var rec = this.GetComponent<RectTransform>();
			rec.anchoredPosition = new Vector2(-0.5f, -16f);
			startSelected = false;
		}

		if(Input.GetKeyUp(KeyCode.UpArrow)){
			var rec = this.GetComponent<RectTransform>();
			rec.anchoredPosition = new Vector2(-0.5f, -13f);
			startSelected = true;
		}

		if(Input.GetKeyUp(KeyCode.Return)){
			if(startSelected){
				SceneManager.LoadScene("Gameplay", LoadSceneMode.Single);
			} else {
				Application.Quit();
			}
		}
	}
}
