﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BlockController : MonoBehaviour
{
    float fall = 0;
    public float fallSpeed = 2;

    float stepTime = 0;
    public float TimeBetweenSteps = 0.5f;

    public bool allowRotation = true;
    public bool limitRotation = false;

    public enum LastKeyPressed
    {
        Left,
        Right
    }

    public LastKeyPressed lastKey;
    void Update()
    {
        Movement();

        if (Input.GetKey(KeyCode.Space))
        {

            this.transform.position += Vector3.down;

            if (!IsValidPosition())
            {
                this.transform.position += Vector3.up;

                Managers.Board.DeleteRow();

                if (Managers.Board.CheckIsAboveGrid(this))
                {
                    Debug.Log("GameOver");
                    Managers.Board.Spawner.canSpawn = false;
                    SceneManager.LoadScene("GameOver");
                }

                this.enabled = false;

                if (Managers.Board.Spawner.canSpawn)
                {
                    Managers.Board.Spawner.SpawnNext();
                }
            }
            else
            {
                Managers.Board.UpdateGrid(this);
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (Time.time - stepTime >= TimeBetweenSteps)
            {
                this.transform.position += Vector3.left;
                lastKey = LastKeyPressed.Left;

                if (!IsValidPosition())
                {
                    this.transform.position += Vector3.right;
                }
                else
                {
                    Managers.Board.UpdateGrid(this);
                }
                stepTime = Time.time;
            }

        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (Time.time - stepTime >= TimeBetweenSteps)
            {
                this.transform.position += Vector3.right;
                lastKey = LastKeyPressed.Right;

                if (!IsValidPosition())
                {
                    this.transform.position += Vector3.left;
                }
                else
                {
                    Managers.Board.UpdateGrid(this);
                }
                stepTime = Time.time;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Rotate();
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {

            if (Time.time - stepTime >= TimeBetweenSteps)
            {
                this.transform.position += Vector3.down;
                fall = Time.time;


                if (!IsValidPosition())
                {
                    this.transform.position += Vector3.up;

                    Managers.Board.DeleteRow();

                    if (Managers.Board.CheckIsAboveGrid(this))
                    {
                        Debug.Log("GameOver");
                        Managers.Board.Spawner.canSpawn = false;
                        SceneManager.LoadScene("GameOver");
                    }

                    this.enabled = false;

                    if (Managers.Board.Spawner.canSpawn)
                    {
                        Managers.Board.Spawner.SpawnNext();
                    }
                }
                else
                {
                    Managers.Board.UpdateGrid(this);
                }

                stepTime = Time.time;
            }
        }
    }

    void Movement()
    {
        if (Time.time - fall >= fallSpeed)
        {
            this.transform.position += Vector3.down;

            if (!IsValidPosition())
            {
                this.transform.position += Vector3.up;

                Managers.Board.DeleteRow();

                if (Managers.Board.CheckIsAboveGrid(this))
                {
                    SceneManager.LoadScene("GameOver");
                    Managers.Board.Spawner.canSpawn = false;
                }

                this.enabled = false;

                if (Managers.Board.Spawner.canSpawn)
                {
                    Managers.Board.Spawner.SpawnNext();
                }

            }
            else
            {
                Managers.Board.UpdateGrid(this);
            }

            fall = Time.time;
        }
    }

    void Rotate()
    {
        if (allowRotation)
        {


            if (limitRotation)
            {
                if (this.transform.rotation.eulerAngles.z >= 90)
                {
                    this.transform.Rotate(0, 0, 90);
                }
                else
                {
                    this.transform.Rotate(0, 0, -90);
                }

            }
            else
            {
                this.transform.Rotate(0, 0, 90);
            }


            if (!IsValidPosition())
            {
                if (limitRotation)
                {
                    if (this.transform.rotation.eulerAngles.z >= 90)
                    {
                        this.transform.Rotate(0, 0, -90);
                    }
                    else
                    {
                        this.transform.Rotate(0, 0, 90);
                    }
                }
                else
                {
                    this.transform.Rotate(0, 0, -90);
                }

            }
            else
            {
                Managers.Board.UpdateGrid(this);
            }
        }

    }

    bool IsValidPosition()
    {
        foreach (Transform block in transform)
        {
            Vector2 pos = Managers.Board.Round(block.position);

            if (!Managers.Board.CheckIsInsideGrid(pos))
            {
                return false;
            }

            if (Managers.Board.GetTransformAtGridPosition(pos) != null && Managers.Board.GetTransformAtGridPosition(pos).parent != transform)
            {
                return false;
            }
        }

        return true;

    }



    void RotateFormat()
    {
        var posCount = 0;
        foreach (Transform block in transform)
        {
            Vector2 pos = Managers.Board.Round(block.position);

            if (!Managers.Board.CheckIsInsideGrid(pos))
            {
                posCount += 1;
            }

        }

        switch (lastKey)
        {
            case LastKeyPressed.Left:
                this.transform.position += new Vector3(posCount, 0, 0);
                Managers.Board.UpdateGrid(this);
                break;
            case LastKeyPressed.Right:
                this.transform.position += new Vector3(-posCount, 0, 0);
                Managers.Board.UpdateGrid(this);
                break;
        }

    }
}

