﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{

    public List<GameObject> blockGroups;

    public int count;

    public bool canSpawn = true;

    public GameObject nextBlock;

    int randomIndex;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        count = 0;
        randomIndex = Random.Range(0, blockGroups.Count);
    }
    public void SpawnNext()
    {
        var instance = Instantiate(blockGroups[randomIndex], transform.position, Quaternion.identity);
        instance.name += "_" + count;
        count++;
        PrepareNextBlock();
    }

    void PrepareNextBlock()
    {
        randomIndex = Random.Range(0, blockGroups.Count);
        var block = Instantiate(blockGroups[randomIndex], nextBlock.transform.position, Quaternion.identity);

        if (nextBlock.transform.GetChild(0) != null)
        {
            Destroy(nextBlock.transform.GetChild(0).gameObject);
        }
        block.transform.SetParent(nextBlock.transform);
        var controller = block.GetComponent<BlockController>();
        controller.enabled = false;
    }
}
