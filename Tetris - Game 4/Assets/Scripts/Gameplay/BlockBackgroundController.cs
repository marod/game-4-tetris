﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBackgroundController : MonoBehaviour {

	float fall;
	float fallSpeed = 0.2f;

	float rotate;

	float rotateSpeed;

	Camera camera;

	// Use this for initialization
	void Start () {
		camera = Camera.main; 
		rotateSpeed =  Random.Range(3f, 5f);
	}
	
	// Update is called once per frame
	void Update () {

		Movement();

	}

	    void Movement()
    {
		if (Time.time - rotate >= rotateSpeed)
        {
			this.transform.Rotate(0,0,90);
            rotate = Time.time;
			rotateSpeed = Random.Range(3f, 5f);
        }

        if (Time.time - fall >= fallSpeed)
        {
            this.transform.position += Vector3.down;
			if(transform.position.y <= -13){
				this.transform.position = new Vector2(Random.Range(-20f, 20f), Random.Range(14f, 18f));
			}

            fall = Time.time;
        }

		
    }
}
