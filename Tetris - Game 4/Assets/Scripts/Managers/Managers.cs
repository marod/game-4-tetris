﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class Managers : MonoBehaviour {

	private static BoardManager _boardManager ;
    public static BoardManager Board
    {
        get
        {
            return _boardManager;
        }
    }

	void Awake()
	{
		_boardManager = this.GetComponent<BoardManager>();
	}
}

