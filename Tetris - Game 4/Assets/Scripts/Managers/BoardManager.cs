﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public static int columns = 11;
    public static int rows = 24;

    List<Vector2> blocksPositions;

    public GameObject GridPrefab;
    public GameObject BoundPrefab;

    public SpawnerController Spawner;

    public static Transform[,] gridBoard = new Transform[columns, rows];

    void Awake()
    {
        blocksPositions = new List<Vector2>();
    }

    void Start()
    {
        CreateGridBoard();
        Spawner.SpawnNext();
    }

    public void CreateGridBoard()
    {

        GameObject board = new GameObject();
        board.name = "Board";
        board.transform.position = new Vector3(0, 0, 0);

        for (int y = rows - 1; y >= -1; y--)
        {
            for (int x = -1; x < columns + 1; x++)
            {
                GameObject instance;

                if (x == -1 || x == columns || y == -1)
                {
                    instance = Instantiate(BoundPrefab, new Vector3(board.transform.position.x + x, board.transform.position.y + y, 0f), Quaternion.identity);
                    instance.transform.SetParent(board.transform);
                }
                else
                {
                    instance = Instantiate(GridPrefab, new Vector3(board.transform.position.x + x, board.transform.position.y + y, 0f), Quaternion.identity);
                    instance.transform.SetParent(board.transform);
                }
            }
        }

    }

    public Vector2 Round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }

    public bool CheckIsInsideGrid(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x <= columns - 1 && (int)pos.y >= 0);
    }

    public void UpdateGrid(BlockController bigBlock)
    {
        for (int y = 0; y < rows; ++y)
        {
            for (int x = 0; x < columns; ++x)
            {
                if (gridBoard[x, y] != null)
                {
                    if (gridBoard[x, y].parent == bigBlock.transform)
                    {
                        gridBoard[x, y] = null;
                    }
                }
            }
        }

        foreach (Transform block in bigBlock.transform)
        {
            var pos = Round(block.position);

            if (pos.y < rows && pos.y > -1)
            {
                gridBoard[(int)pos.x, (int)pos.y] = block;
            }

        }
    }

    public Transform GetTransformAtGridPosition(Vector2 pos)
    {
        if (Mathf.Abs(pos.y) > rows)
        {
            return null;
        }
        else
        {
            return gridBoard[(int)pos.x, (int)pos.y];
        }
    }

    public bool IsFullRowAt(int y)
    {
        for (int x = 0; x < columns; x++)
        {
            if (gridBoard[x, y] == null)
            {
                return false;
            }
        }

        return true;
    }

    public void DeleteBlockAt(int y)
    {
        for (int x = 0; x < columns; x++)
        {
            Destroy(gridBoard[x, y].gameObject);
            gridBoard[x, y] = null;
        }
    }

    public void MoveRowDown(int y)
    {
        for (int x = 0; x < columns; x++)
        {
            if (gridBoard[x, y] != null)
            {
                gridBoard[x, y - 1] = gridBoard[x, y];

                gridBoard[x, y] = null;

                gridBoard[x, y - 1].position += Vector3.down;
            }
        }
    }

    public void MoveAllRowsDown(int y)
    {
        for (int i = y; i < rows; i++)
        {
            MoveRowDown(i);
        }
    }

    public void DeleteRow()
    {
        for (int y = 0; y < rows; ++y)
        {
            if (IsFullRowAt(y))
            {
                DeleteBlockAt(y);

                MoveAllRowsDown(y + 1);

                --y;
            }
        }
    }

    public bool CheckIsAboveGrid(BlockController blockBig)
    {
        for (int x = 0; x < columns; x++)
        {
            foreach (Transform block in blockBig.transform)
            {
                Vector2 pos = Round(block.position);

                if (pos.y >= rows - 5)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
